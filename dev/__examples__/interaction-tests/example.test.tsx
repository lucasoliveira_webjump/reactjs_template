import 'jest-enzyme'
import React from 'react'
import { mount } from 'enzyme'

it('example', function() {
  function User(props: any) {
    return <span className={props.className}>User {props.index}</span>
  }

  function Fixture() {
    return (
      <div>
        <ul>
          <li>
            <User index={1} className="userOne" />
          </li>
          <li>
            <User index={2} className="userTwo" />
          </li>
        </ul>
      </div>
    )
  }

  const wrapper = mount(<Fixture />) // mount/render/shallow when applicable

  expect(wrapper).toContainMatchingElement('.userOne')
  expect(wrapper).not.toContainMatchingElement('.userThree')
})
