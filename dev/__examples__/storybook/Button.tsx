import React from 'react'
import styled from '@xstyled/styled-components'

const Container = styled.a`
  display: flex;
  flex: 1;
  background: red;
  padding: 10px;
`

const Button = (props: any) => <Container>{props.children}</Container>
export default Button
