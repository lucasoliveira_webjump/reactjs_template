import sum from './sum'

it('should sum two numbers', () => {
  const response = sum(1, 2)
  const expectedResponse = 3
  return expect(response).toEqual(expectedResponse)
})
