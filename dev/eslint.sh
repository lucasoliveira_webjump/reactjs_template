#!/usr/bin/env bash

# fetch comparable version
COMPARABLE_VERSION=$(git show-branch -a | grep '\*' | grep -v `git rev-parse --abbrev-ref HEAD` | head -n1 | sed 's/.*\[\(.*\)\].*/\1/' | sed 's/[\^~].*//')
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch origin

# find modified files
FILES=""
for file in $(git diff --name-only HEAD $(git merge-base HEAD $COMPARABLE_VERSION) -- src)
do
  if [[ -f "$file" && ${file: -4} == ".tsx" || -f "$file" && ${file: -3} == ".ts" || -f "$file" && ${file: -3} == ".js" ]]; then
    FILES+="$file "
  fi
done

# run lint
if [ ! -z "$FILES" ]; then
  yarn eslint $FILES
fi
