#!/usr/bin/env bash

set +e

if [ "$1" == "commit" ]; then
  echo "commiting snapshot tests"
  yarn jest dev/storyshots.test.ts -u --ci --silent --noStackTrace --forceExit --detectOpenHandles
else
  echo "running snapshot tests"
  yarn jest dev/storyshots.test.ts --ci --silent --noStackTrace --forceExit --detectOpenHandles
fi

set -e
