import initStoryshots from '@storybook/addon-storyshots'
import styleSheetSerializer from 'jest-styled-components/src/styleSheetSerializer'

initStoryshots({ snapshotSerializers: [styleSheetSerializer] })
