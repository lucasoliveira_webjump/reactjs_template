#!/usr/bin/env bash

yarn jest ../src/ --ci --silent --noStackTrace --forceExit --detectOpenHandles
