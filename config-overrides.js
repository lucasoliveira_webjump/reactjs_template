// eslint-disable-next-line @typescript-eslint/no-var-requires
const { override, addLessLoader, addDecoratorsLegacy, disableEsLint } = require('customize-cra')

module.exports = override(disableEsLint(), addDecoratorsLegacy(), addLessLoader({ javascriptEnabled: true }))
